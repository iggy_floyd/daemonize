#!/bin/sh

DAEMON=$CAYLEYRESPWAN
DAEMON_PATTERN1="cayley"
DAEMON_PATTERN2="http"
KILLWORD="cayleykill"




. /lib/lsb/init-functions



do_start () {

        log_daemon_msg "Starting $0"

        [ -z "$DAEMON" ] &&  (log_end_msg 1; return 1;)
        echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null
        if [ $? -eq 0 ]
        then
                log_end_msg 1;
                return 1;
        else 
                 echo $DAEMON | bash;
                 log_end_msg $?
        fi



}



do_stop () {
	log_daemon_msg "Stopping $0"

	[ -n "$DAEMON" ] &&  
	[  `which nc` ] && 
	echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null &&
	echo  $KILLWORD | nc -q1  localhost 9760 &&
	log_end_msg $? ||
	log_end_msg 1




}


case "$1" in
 
start|stop)
	do_${1}
	;;
 
restart|reload|force-reload)
	do_stop
	do_start
	;;
 
status)
	log_daemon_msg  "Status of $0"

	[ -n "$DAEMON" ]  && echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null &&
        log_end_msg $? ||
        log_end_msg 1

;;
*)
echo "Usage: $0 {start|stop|restart|status}"
exit 1
;;
 
esac
exit 0
